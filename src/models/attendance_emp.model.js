var dbConn  = require('../../config/db.config');

var attendance = function(attendance){
    this.aid     =   attendance.aid;
    this.attendance_type  =   attendance.attendance_type;
    this.attendance_date  =   attendance.attendance_date;
    this.attendance_time  =   attendance.attendance_time;
    this.employee_id  =   attendance.employee_id;
    
}

// get all attendance records
attendance.getEmployeeAttendance = (result) =>{
    dbConn.query('SELECT * FROM attendance_table', (err, res)=>{

        if(err){
            console.log('Error while fetching attendance', err);
            result(null,err);
        }else{
            console.log('Attendance results found!');
            result(null,res);
        }
    })
}

// get attendance by ID from DB
attendance.getAttendanceByID = (id, result)=>{
    dbConn.query('SELECT * FROM attendance_table WHERE employee_id=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching attendance by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new attendance record
attendance.createNewAttendance = (attendanceReqData, result) =>{
    dbConn.query('INSERT INTO attendance_table SET ? ', attendanceReqData, (err, res)=>{
        if(err){
            console.log(err.message)
            console.log('Error while creating new attendance record!');
            result(null, err);
        }else{
            console.log('attendance record created successfully');
            result(null, res)
        }
    })
}

// update attendance
attendance.updateAttendance = (id, attendanceReqData, result)=>{
    dbConn.query("UPDATE attendance_table SET aid=?, attendance_type=?, attendance_date=?, attendance_time=? where employee_id=?", [attendanceReqData.aid, attendanceReqData.attendance_type,attendanceReqData.attendance_date, attendanceReqData.attendance_time, id], (err, res)=>{
        if(err){
            console.log('Error while updating the attendance');
            result(null, err);
        }else{
            console.log("attendance updated successfully");
            result(null, res);
        }
    });
}

// delete attendance
attendance.deleteAttendance = (id, result)=>{
    dbConn.query('DELETE FROM attendance_table WHERE aid=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the attendance');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = attendance;