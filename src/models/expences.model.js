var dbConn  = require('../../config/db.config');

var expences = function(expences){
    this.eid     =   expences.eid;
    this.expence_employees_id  =   expences.expence_employees_id;
    this.expence_amount  =   expences.expence_amount;
    this.expence_reason  =   expences.expence_reason;
    
}

// get all expence records
expences.getExpences = (result) =>{
    dbConn.query('SELECT * FROM expence_table', (err, res)=>{

        if(err){
            console.log('Error while fetching expences', err);
            result(null,err);
        }else{
            console.log('Expence results found!');
            result(null,res);
        }
    })
}

// get expence by ID from DB
expences.getExpencesByID = (id, result)=>{
    dbConn.query('SELECT * FROM expence_table WHERE eid=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching expences by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new expence record
expences.createNewExpences = (expenceReqData, result) =>{
    console.log(expenceReqData)
    dbConn.query('INSERT INTO expence_table SET ?', expenceReqData, (err, res)=>{
        if(err){
            console.log(err.message)
            console.log('Error while creating new expence record!');
            result(null, err);
        }else{
            console.log('expence record created successfully');
            result(null, res)
        }
    })
}

// update expence
expences.updateExpences = (id, expenceReqData, result)=>{
    dbConn.query("UPDATE expence_table SET eid=?, expence_employees_id=?, expence_amount=?, expence_reason=? where eid=?", [expenceReqData.eid, expenceReqData.expence_employees_id,expenceReqData.expence_amount, expenceReqData.expence_reason, id], (err, res)=>{
        if(err){
            console.log('Error while updating the expence');
            result(null, err);
        }else{
            console.log("expence updated successfully");
            result(null, res);
        }
    });
}

// delete expence
expences.deleteExpences = (id, result)=>{
    dbConn.query('DELETE FROM expence_table WHERE eid=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the expence');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = expences;