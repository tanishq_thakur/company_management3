
const attendanceModel = require('../models/attendance_emp.model');
const JOI = require('joi');
const schema = JOI.object().keys({
    aid: JOI.number().integer(),
    attendance_type: JOI.number().integer(),
    attendance_date: JOI.string().trim(),
    attendance_time: JOI.string().trim(),
    employee_id: JOI.number().integer()
})
    
const validateBody = (req)=>{
   
}


// get all attendance list
exports.getEmployeeAttendance = (req, res)=> {
    //console.log('here all attendance list');
    attendanceModel.getEmployeeAttendance((err, att) =>{
        console.log('We are here');
        if(err)
        res.send(err);
        console.log('Attendance', att);
        res.send(att)
    })
}

// get attendance by ID
exports.getAttendanceByID = (req, res)=>{
    console.log('get attendance by id');
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
    // console.log(JOI.assert(req.params.id, JOI.number(),'yes'))
    // const result = await validate
    attendanceModel.getAttendanceByID(req.params.id, (err, att)=>{
        if(att===[]){
            res.send({success:false,error:"Id does not exist"})
        }else{
        if(err)
        res.send(err);
        console.log('Employee attendance',att);
        res.send(att);
    }})
}


// create new attendance record
exports.createNewAttendance = async (req, res) =>{
    try{
        const result =  await schema.validateAsync(req.body)
        // return result
    }catch(err){
        res.json({success:false, error:err.message})
        console.log(err)
    }

    const attendanceReqData = new attendanceModel(req.body);
    console.log('attendanceReqData', attendanceReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        attendanceModel.createNewAttendance(attendanceReqData, (err, att)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'attendance Created Successfully', data: att.insertId})
        })
    }
}


// update attendance
exports.updateAttendance =  (req, res)=>{
    validateBody(req.body)

    const attendanceReqData = new attendanceModel(req.body);
    console.log('attendanceReqData update', attendanceReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        attendanceModel.updateAttendance(req.params.id, attendanceReqData, (err, att)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'attendance updated Successfully'})
        })
    }
}


// delete attendance
exports.deleteAttendance = (req, res)=>{
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
    attendanceModel.deleteAttendance(req.params.id, (err, att)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'attendance deleted successully!'});
    })}
}}