const express = require('express');
const router = express.Router();

const expencesController = require('../controllers/expences.controller');

// get expences
router.get('/', expencesController.getExpences);

// get expences by ID
router.get('/:id',expencesController.getExpencesByID);

// create new expences record
router.post('/', expencesController.createNewExpences);

// update expences
router.put('/:id', expencesController.updateExpences);

// delete expences
router.delete('/:id',expencesController.deleteExpences);

module.exports = router;