const express = require('express');
const router = express.Router();

const attendanceController = require('../controllers/attendance_emp.controller');

// get attendance
router.get('/', attendanceController.getEmployeeAttendance);

// get attendance by ID
router.get('/:id',attendanceController.getAttendanceByID);

// create new attendance record
router.post('/', function(req,res){
    attendanceController.createNewAttendance});

// update attendance
router.put('/:id', (req,res)=>{attendanceController.updateAttendance});

// delete attendance
router.delete('/:id',(req,err)=>{attendanceController.deleteAttendance});

module.exports = router;